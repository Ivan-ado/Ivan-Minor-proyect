import sys
import requests
import pickle
import json
from tkinter import *
import tkinter as tk
from PIL import ImageTk
from PIL import Image
from tkinter import filedialog
import io
import requests
from io import BytesIO
import cognitive_face as CF
from PIL import Image, ImageDraw, ImageFont

#creator of the principal class Person
class Person():
    def __init__(self,identification, personid,name, age, gender, photoroute):
        self.identification = identification
        self.personid = personid
        self.name = name
        self.age = age
        self.gender = gender
        self.photoroute = photoroute
    """ 
    def printpeople(self):
        print("los datos de la persona son")
        print(self.identification)
        print(self.personid)
        print(self.name)
        print(self.age)
        print(self.gender)
        print(self.photoroute)
    """
#creator of the subclass Familly for the groups
class Familly(Person):
    def __init_subclass__(self,identification,personid,name,age,gender,photoroute,numembers,relation):

        Person.__init__(self,identification,personid,name,age,gender,photoroute)
        self.numembers = numembers
        self.relation = relation
#creator of the subclass Friends for the groups
class Friends(Person):

    def __init_subclass__(self,identification,personid,name,age,gender,photoroute,numembers,relation):

        Person.__init__(self,identification,personid,name,age,gender,photoroute)
        self.numembers = numembers
        self.relation = relation

#creator of the class Famous for the groups
class Famous(Person):

    def __init_subclass__(self,identification,personid,name,age,gender,photoroute,numembers,relation):

        Person.__init__(self,identification,personid,name,age,gender,photoroute)
        self.numembers = numembers
        self.relation = relation

#function to create the graphic interfase of the app
class Ventana(tk.Frame):

    def __init__(self, master=None):
        Frame.__init__(self, master)
        self.master = master
        self.iniciar_menu() 

        self.label = tk.Label(self, text="Sistema de reconocimiento facial")
        self.label.pack(padx=20, pady=20)

    #function to start a menu to work with in the app
    def iniciar_menu(self):
        self.master.title("Mi imagen")
        self.pack(fill=BOTH, expand=1)

        menu = Menu(self.master)
        self.master.config(menu=menu)

        ventanas = Menu(menu)
        ventanas.add_command(label="Agregadatospersona", command=self.agregardatos)
        ventanas.add_command(label="Buscapersonaporimagen", command=self.infoimagencuadro)
        
        menu.add_cascade(label="Opciones", menu=ventanas)
    #function to close open windows
    def salir(self,*argumentos):
        print("cerrando registro")

    #function to save the data of a person in an object and then in a bin documment
    def Guardar(self,*argumentos):
        ruta= argumentos[0]
        name= argumentos[1]
        identificacion= argumentos[2]
        information=emotions(ruta)
        for a in information:
            carac=a["faceAttributes"]
            faceId=a["faceId"]
            age=carac["age"]
            gender=carac["gender"]
            with open("archivo4.bin", "wb") as f:
                name=Person(identificacion,carac,name,age,gender,ruta)
                pickle.dump(name, f)

    #function to identify the correct data in the data base
    def buscadatos(self,*argumentos):
        with open("archivo4.bin", "rb") as f:
            name_leer = pickle.load(f)
            rute=name_leer.photoroute   
            if rute == argumentos[0]:
                persona = name_leer
                return persona
    
    
    


    #Function to show image in the graphic interface
    def show_image(self,picture): #>><<><<>>><<<><<>><<>>>>><<><<
        faces=emotions(picture)
        image = Image.open(picture)
        for face in faces:
            fa = face['faceRectangle']
            top = fa['top'] 
            left = fa['left']  
            width = fa['width'] 
            height = fa['height']
            draw = ImageDraw.Draw(image)
            draw.rectangle((left, top, left+width, top+ height), outline= "red", width = 4)
        image.show(image)  


    #window to add data to a person
    def agregardatos(self):
        agregardatos = tk.Toplevel()
        agregardatos.title("agrega datos a personas")

        agregardatos.lb_rutaimage = tk.Label(agregardatos, text= "ingrese la ruta de la imagen: ")
        agregardatos.lb_nombre = tk.Label(agregardatos, text= "ingrese el nombre de la persona: ")
        agregardatos.lb_identifica = tk.Label(agregardatos, text= "ingrese la identificacion de la persona: ")

        agregardatos.sv_rutaimage = tk.StringVar()
        agregardatos.tb_rutaimage = tk.Entry(agregardatos, textvariable = agregardatos.sv_rutaimage, width=40)

        agregardatos.sv_nombre = tk.StringVar()
        agregardatos.tb_nombre = tk.Entry(agregardatos, textvariable = agregardatos.sv_nombre, width=40)

        agregardatos.sv_identifica = tk.StringVar()
        agregardatos.tb_identifica = tk.Entry(agregardatos, textvariable = agregardatos.sv_identifica, width=40)

        agregardatos.b_salir = tk.Button(agregardatos, text = "Salir", command = lambda: self.salir(agregardatos.destroy()))

        agregardatos.b_guardar = tk.Button(agregardatos, text = "Guardar", command = lambda: self.Guardar(
            agregardatos.sv_rutaimage.get(),
            agregardatos.sv_nombre.get(),
            agregardatos.sv_identifica.get(),
            agregardatos.sv_rutaimage.set(""),
            agregardatos.sv_nombre.set(""),
            agregardatos.sv_identifica.set("")
        ))

        agregardatos.lb_rutaimage.grid(column=0, row=0, padx=(20,10))
        agregardatos.lb_nombre.grid(column=0, row=1, padx=(20,10))
        agregardatos.lb_identifica.grid(column=0, row=2, padx=(20,10))

        agregardatos.tb_rutaimage.grid(column=1, row=0, pady=5, columnspan=2,padx=(20,10))
        agregardatos.tb_nombre.grid(column=1, row=1, pady=5, columnspan=2, padx=(20,10))
        agregardatos.tb_identifica.grid(column=1, row=2, pady=5, columnspan=2, padx=(20,10))

        agregardatos.b_salir.grid(column=1, row=3, pady=15)
        agregardatos.b_guardar.grid(column=0, row=3, pady=15)
    #window to show data of a person with the image
    def infoimagencuadro(self):
        infoimagencuadro = tk.Toplevel()
        infoimagencuadro.title("busca a persona por su imagen")

        infoimagencuadro.lb_rutaimage = tk.Label(infoimagencuadro, text= "ingrese la ruta de la imagen a buscar: ")
        infoimagencuadro.sv_rutaimage = tk.StringVar()
        infoimagencuadro.tb_rutaimage = tk.Entry(infoimagencuadro, textvariable = infoimagencuadro.sv_rutaimage, width=40)

        infoimagencuadro.b_salir = tk.Button(infoimagencuadro, text = "Salir", command = lambda: self.salir(infoimagencuadro.destroy()))
        infoimagencuadro.b_buscar = tk.Button(infoimagencuadro, text = "Buscar", command = lambda: self.buscadatos(self.muestradatos(infoimagencuadro.sv_rutaimage.get())))
    
        infoimagencuadro.lb_rutaimage.grid(column=0, row=0, padx=(20,10))
        infoimagencuadro.tb_rutaimage.grid(column=1, row=0, columnspan=2, padx=(20,10))

        infoimagencuadro.b_salir.grid(column=1, row=3, pady=15)
        infoimagencuadro.b_buscar.grid(column=0, row=3, pady=15)

    #window to show the data found about the person in the image
    def muestradatos(self, persona):

        muestradatos = tk.Toplevel()
        muestradatos.title("datos encontrados")

        muestradatos.lb_nombre = tk.Label(muestradatos, text = "Nombre: ")
        muestradatos.lb_dnombre = tk.Label(muestradatos, text = persona.name)
        muestradatos.lb_identifica = tk.Label(muestradatos, text = "Identificacion: ")
        muestradatos.lb_didentifica = tk.Label(muestradatos, text = persona.identification)
        muestradatos.lb_edad = tk.Label(muestradatos, text = "Edad: ")
        muestradatos.lb_dedad = tk.Label(muestradatos, text= persona.age)
        muestradatos.lb_genero = tk.Label(muestradatos, text = "Genero: ")
        muestradatos.lb_dgenero = tk.Label(muestradatos, text = persona.gender)

        muestradatos.b_salir = tk.Button(muestradatos, text = "Salir", command = lambda: self.salir(muestradatos.destroy()))
        muestradatos.b_mostrarimage = tk.Button(muestradatos, text = "Imagen", command = lambda: self.show_image(persona.photoroute))

        muestradatos.lb_nombre.grid(column=0, row=0,padx=(20,10))
        muestradatos.lb_dnombre.grid(column=0, row=1,padx=(20,10))
        muestradatos.lb_identifica.grid(column=0, row=2,padx=(20,10))
        muestradatos.lb_didentifica.grid(column=0, row=3,padx=(20,10))
        muestradatos.lb_edad.grid(column=0, row=4,padx=(20,10))
        muestradatos.lb_dedad.grid(column=0, row=5,padx=(20,10))
        muestradatos.lb_genero.grid(column=0, row=6,padx=(20,10))
        muestradatos.lb_dgenero.grid(column=0, row=7,padx=(20,10))

        muestradatos.b_salir.grid(column=1, row=8, pady=15)
        muestradatos.b_mostrarimage.grid(column=0, row=8, pady=15)

        



#connection with the microsoft azure server

subscription_key = None

SUBSCRIPTION_KEY = 'bd6c79bb7a4c418a9444d2cf7304de08'
BASE_URL = 'https://proyectocarabrrr.cognitiveservices.azure.com/face/v1.0/'
CF.BaseUrl.set(BASE_URL)
CF.Key.set(SUBSCRIPTION_KEY)


#function to get the information from the image with the route of it
def emotions(picture):
    #headers = {'Ocp-Apim-Subscription-Key': 'e70e11c9cb684f21b8b37313fd60e5bc'}
    image_path = picture
    #https://docs.microsoft.com/en-us/azure/cognitive-services/computer-vision/quickstarts/python-disk
    # Read the image into a byte array
    image_data = open(image_path, "rb").read()
    headers = {'Ocp-Apim-Subscription-Key': SUBSCRIPTION_KEY,
    'Content-Type': 'application/octet-stream'}
    params = {
        'returnFaceId': 'true',
        'returnFaceLandmarks': 'false',
        'returnFaceAttributes': 'age,gender,headPose,smile,facialHair,glasses,emotion,hair,makeup,occlusion,accessories,blur,exposure,noise',
    }
    response = requests.post(
                             BASE_URL + "detect/", headers=headers, params=params, data=image_data)
    analysis = response.json()
    #quitar el # para que el sistema imprima la lista con los datos
    #print(analysis)
    return analysis
#function to create a group of people
def creategroup(groupid,groupname):

    CF.person_group.create(groupid,groupname)
    print("grupo creado")
#function to create a person to add to the group
def createpersonforgroup(persona,group_id):
    
    identi= persona.identification
    idpersona= persona.personid
    name= persona.name
    age= persona.age 
    gender= persona.gender

    user_data = [identi, idpersona, age, gender]
    photoroute = persona.photoroute

    response = CF.person.create(group_id,name,user_data)

    peopleid= response["personId"]
    
    CF.person.add_face(photoroute, group_id, peopleid)

def informationclassperson():

    print("digite la ruta hacia la imagen de la persona")
    route= input() 
    print("digite la identificacion de la persona")
    identi= input()
    print("digite el nombre de la persona")
    name= input()  

    information= emotions(route)
    for a in information:
        carac=a["faceAttributes"]
        faceId=a["faceId"]
        age=carac["age"]
        gender=carac["gender"]

        human = Person(identi,faceId,name,age,gender,route)
        createpersonforgroup(human,40)

if __name__=="__main__":
    root = tk.Tk()
    root.geometry("400x300")
    lista_estudiantes = []
    registro_actual = 0

    main = Ventana(root)
    main.pack(fill="both", expand=True)

root.mainloop()